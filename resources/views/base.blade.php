<!DOCTYPE html>
<html class="no-js" ng-app="app">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="favicon.png">
        <title>Editorial</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="{{ asset('css/ionicons.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="{{ asset('css/slider.css')}}">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{ asset('css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css')}}">
        <!-- template main css file -->
        <link rel="stylesheet" href="{{ asset('css/main.css')}}">
        <!-- responsive css -->
        <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
        <link rel="stylesheet" href="{{ asset('lightGallery/css/lightgallery.min.css') }}">

        <!-- modernizr js -->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <!-- wow js -->

        <!-- jquery -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- owl carouserl js -->
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>

        <!-- bootstrap js -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/wow.min.js') }}"></script>
        <!-- slider js -->
        <script src="{{ asset('js/slider.js') }}"></script>
        <script src="{{ asset('js/jquery.fancybox.js') }}"></script>
        <!-- template main js -->
        <script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('js/angular.min.js') }}"></script>
        <script src="{{ asset('js/controller.js') }}"></script>
        <script src="{{ asset('lightGallery/js/lightgallery.min.js') }}"></script>
        <script src="{{ asset('lightGallery/js/lg-thumbnail.min.js') }}"></script>
        <script src="{{ asset('lightGallery/js/lg-fullscreen.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                    $("#lightgallery").lightGallery({thumbnail:true}); 
                });
        </script>

    </head>
    <body>
        
        @include('header')
        
       
            @yield('content')
       
        @include('action')
        @include('footer')
                
        </body>
    </html>