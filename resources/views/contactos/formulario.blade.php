<section id="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="block">
                    <h2 class="subtitle wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Escribenos</h2>
                    <p class="subtitle-des wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                        Pueder solicitar nuestros libros por correo.
                    </p>
                    <div class="contact-form">
                        <form id="contact-form" role="form" method="POST" action="/correo">
                              {!! csrf_field() !!}

                                <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                                  <input type="text" placeholder="Nombre" class="form-control" required name="nombre">
                                </div>
                              
                                <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".8s">
                                    <input type="email" placeholder="Correo" class="form-control" required name="email">
                                </div>
                                
                                <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="1.2s">
                                    <textarea rows="6" placeholder="Mensaje" class="form-control" required name="mensaje"></textarea>    
                                </div>
                                <div ng-show="emailErrores" class="alert alert-info" role="alert">
                                    <p ng-bind="emailErrores"></p>
                                </div>
                                <div id="submit" class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay="1.4s">
                                    <input type="submit" id="contact-submit" class="btn btn-default btn-send" value="Enviar">
                                </div>
                        </form>
                        @if (isset($msj))
                        <div class="col-md-8 col-md-offset-2">
                        <br>
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" role="alert" aria-hidden="true">&times;</button>
                                <strong>{{$msj}}</strong>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="map-area">
                    <h2 class="subtitle  wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">Encuentranos</h2>
                    <p class="subtitle-des wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                        Puedes visitarnos y adquirir nuestros libros.                        
                    </p>
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2370.856828099517!2d-88.90444805149691!3d13.852247726418621!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2ssv!4v1473792475955" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                   {{--      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15495.184857749486!2d-88.91494998066732!3d13.851267678341745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f635cf6343bae31%3A0x2017111df20cf3fa!2sTejutepeque!5e0!3m2!1ses-419!2ssv!4v1478902081400" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row address-details">
            <div class="col-md-3">
                <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                    <i class="ion-android-pin"></i>
                    <h5>Tejutepeque, Cabañas, El Salvador.</h5>
                </div>
            </div>
            <a href="https://www.facebook.com/elsalvadorysucultura/?fref=ts" target="_black">
            <div class="col-md-3">
                <div class="address wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".5s">
                    <i class="ion-social-facebook"></i>
                    <h5>@elsalvadorysucultura</h5>
                </div>
            </div>
            </a>
            <a href="mailto:mauricio.f10@hotmail.com">
            <div class="col-md-3">
                <div class="email wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".7s">
                    <i class="ion-android-mail"></i>
                    <h5>mauricio.f10@hotmail.com</h5>
                </div>
            </div>
            </a>
            <a href="tel:+503 7706-0410">
            <div class="col-md-3">
                <div class="phone wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".9s">
                    <i class="ion-social-whatsapp"></i>
                    <h5>+(503) 7706-0410</h5>
                </div>
            </div>
            </a>
        </div>
    </div>
</section>