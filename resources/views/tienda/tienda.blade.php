<section class="works service-page">
    <div class="container">
        <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">
            Estos son nuestros productos
        </h2>
            <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms">
                Aliquam lobortis. Maecenas vestibulum mollis diam. Pellentesque auctor neque nec urna. Nulla sit amet est. Aenean posuere <br> tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus.
            </p>
        <div class="row">
            @foreach($libros as $libros)
                <div class="col-md-3  col-sm-6 col-xs-12">
                    <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                        <div class="img-wrapper">
                            <img ng-src="images/libros/{{$libros['img']}}.jpg" class="img-responsive" alt="this is a title" >
                            <div class="overlay">
                                <div class="buttons">
                                    <a rel="gallery" class="fancybox" href="images/libros/{{$libros['img']}}.jpg">Ver</a>
                                    <a target="_blank" href="single-portfolio.html">Detalles</a>
                                </div>
                            </div>
                        </div>
                        <figcaption>
                        <h4>
                        <a href="#">
                            {{$libros['nombre']}}
                        </a>
                        </h4>
                        <p>
                            {{$libros['titulo']}}
                        </p>
                        </figcaption>
                    </figure>
                </div>
            @endforeach
    </div>
</section>