<section id="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
                        Escribenos
                    </h1>
                    <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">
                        Solicita nuestras libros y apoyaras nuestro trabajo,<br>
                        cuentanos en que te podemos ayudar
                    </p>
                    <a href="{{ route('contactos') }}" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Comprar libros</a>
                </div>
            </div>
            
        </div>
    </div>
</section>