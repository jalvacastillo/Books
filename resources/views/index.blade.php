@extends('base')

@section('content')

 <section id="hero-area" >
    
    @include('home.slider')

    @include('home.about')

    @include('home.libros')

    {{-- @include('home.departamentos') --}}

 </section>

@endsection