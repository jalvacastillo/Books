@extends('base')

@section('content')



    @include('nosotros.titulo')
    @include('nosotros.descripcion')
    @include('nosotros.caracteristicas')
    @include('nosotros.equipo')
    @include('nosotros.clientes')

@endsection