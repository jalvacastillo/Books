<section id="team">
    <div class="container">
        <div class="row">
            <h2 class="subtitle text-center">Nuestro equipo</h2>
            <div class="col-md-3">
                <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                    <div class="team-img">
                        <img src="images/team/team-1.jpg" class="team-pic" alt="">
                    </div>
                    <h3 class="team_name">Mauricio Flores</h3>
                    <p class="team_designation">Escritor</p>
                </div>
            </div>
            <div class="col-md-9">
                <div class="team-member wow fadeInLeft" data-wow-duration="500ms" data-wow-delay=".3s">
                    <p style="text-align: justify; color: #727272;">
                        Nace el 30 de septiembre de 1967 en Tejutepeque departamento de Cabañas, donde termina sus estudios primeros en centros públicos locales, luego afectado por la guerra civil al cerrar centros educativos por la falta de profesores, continúa estudiando en Ilobasco donde termina su bachillerato Comercial, Opción: Contaduría, en el año de 1986, inmediatamente inicia a trabajar para contribuir al sostenimiento de su hogar y continuar estudios superiores, inscribiéndose inicialmente en la Universidad de El Salvador en la Facultad de Economía estudiando Administración de Empresas, pero siente una vocación a las letras, para ese entonces ya entre sus amigos y familiares ya se le consideraba además como un escritor, poeta nato y decide cambiarse a estudiar comunicaciones. <br><br> Para el año 1990 se cambia a la Universidad Católica UCA donde es uno de los primeros estudiantes que inician la carrera de Comunicaciones, a la vez que ya se ve enrolado en problemas sentimentales y se siente afectado por entretenciones propias de todo joven que quiere disfrutar de la vida en todos los placeres le sea posible, así es como dice él, “un día en silencio en un puerto vacío y solitario bajó del barco de sus estudios sin equipaje con los sueños rotos y se puso a buscar sirenas donde no las encontraría, y ahí halló la inspiración para escribir poemas de amor aun inéditos. <br><br> Su experiencia laboral por casi 20 años en las áreas de auditoria y contabilidad en todas las áreas administrativas le han sido útiles como para valorar y ver hasta donde se puede llegar en un país donde hay que construirse los sueños y oportunidades para contribuir en lo mínimo como agradecimiento a la vida por todo lo vivido, en lo social desde sus 20 años de edad, ha participado en movimientos juveniles principalmente católicos, buscando oportunidades para favorecer a quienes más lo necesitan, es padre soltero de dos jóvenes, y su experiencia en publicación de boletines y revista le inspira a peregrinar en la búsqueda de información e investigación de los aspectos socio culturales del país, para recopilar datos e información en libros que sirvan no solo para dar a conocer la cultura del país, sino también para amar a nuestro El Salvador.            
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>