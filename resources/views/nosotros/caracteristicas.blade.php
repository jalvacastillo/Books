<section class="about-feature clearfix">
    <div class="container-fluid">
        <div class="row">
            <div class="block about-feature-1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">
                <h2 class="item_title">Misión</h2>
                <p>
                    Hacer investigación cultural en todos los aspectos, despertando el interés por conocer de nuestra historia en todas las personas que amamos El Salvador como nuestro país, siendo objetivos e independientes para llegar a nuestros clientes una gama de productos que su sellos de calidad sea el orgullo e identidad de ser salvadoreño, marcando así la diferencia de ser portadores únicos y permanentes de hacer patria por amor.
                </p>
            </div>
            <div class="block about-feature-2 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                <h2 class="item_title">Visión</h2>
                <p>
                    Ser lideres innovadores en la promoción cultural del pais, posicionandonos como una fuente inmediata que contribuye a inrequeser el espiritu de quienes desean alimentarce mediante la historia, llegando a nuestros hermanos sin importar donde se encuentren llevandoles la información que necesitan en la forma que lo exijan.
                </p>
            </div>
            <div class="block about-feature-3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".7s">
                <h2 class="item_title">Objetivo</h2>
                <p>
                    Investigar, rescatar y documentar en todos los medios posibles la historia de El Salvador para compartirla y así contribuir a despertar conciencia mejorando la calidad de vida a través de la cultura
                </p>
            </div>
        </div>
    </div>
</section>
