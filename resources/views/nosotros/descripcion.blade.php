<section class="company-description">
    <div class="container">
        <div class="row">
            <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                <img src="images/about.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-md-6">
                <div class="block">
                    <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">
                    Porque somos diferentes
                    </h3>
                    <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                       Nuestra empresa es de carácter socio cultural, único por su naturaleza en el quehacer del rescate y promoción de la cultura, costumbres y tradiciones, contribuyendo así a cultivar valores que fortalecen la identidad cuscatleca, que da frutos de unidad a nuestro El Salvador entre sus hijos que hacen país dentro y fuera de nuestras fronteras
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>