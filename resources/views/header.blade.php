<header id="top-bar" class="navbar-fixed-top animated-header">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->
            
            <!-- logo -->
            {{-- <div class="navbar-brand">
                <a href="{{ route('home') }}" >
                    <img src="/images/logo.png" alt="">
                </a>
            </div> --}}
            <!-- /logo -->
        </div>
        <!-- main menu -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <div class="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ route('home') }}" >inicio</a>
                    </li>
                    <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                    {{-- <li><a href="{{ route('cultura') }}">Cultura</a></li> --}}
                    <li><a href="{{ route('tienda') }}">Tienda</a></li>
                   {{--  <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Galeria <span class="caret"></span></a>
                        <div class="dropdown-menu">
                            <ul>
                                <li><a href="{{ route('fotos') }}">Fotos</a></li>
                                <li><a href="{{ route('videos') }}">Videos</a></li>
                            </ul>
                        </div>
                    </li> --}}
                    <li><a href="{{ route('contactos') }}">Contactos</a></li>
                </ul>
            </div>
        </nav>
        <!-- /main nav -->
    </div>
</header>