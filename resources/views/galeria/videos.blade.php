@extends('base')

@section('content')

    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>Galeria de Videos</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ route('home') }}">
                                    <i class="ion-ios-home"></i>
                                    Inicio
                                </a>
                            </li>
                            <li class="active">Galeria de videos</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="gallery" class="gallery" style="margin-bottom:50px;" ng-controller="VideosCtrl">
        <div class="container">
            <div class="row">
                <div class="col-xs-6" ng-repeat="video in videos">
                    <h3 ng-bind="video.snippet.title"></h3>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" video dynamic-url-src="@{{video.id.videoId}}" allowfullscreen></iframe>
                    </div> 
                </div>
            </div>
        </div>
    </section>

@endsection