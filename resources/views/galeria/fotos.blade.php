@extends('base')

@section('content')

    <section class="global-page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h2>Galeria de Fotos</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ route('home') }}">
                                    <i class="ion-ios-home"></i>
                                    Inicio
                                </a>
                            </li>
                            <li class="active">Galeria de fotos</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="gallery" class="gallery" style="margin-bottom:50px;">
        <div class="container">
            <div class="row">
                <div id="lightgallery">
                    <?php
                        $ruta = "images/galeria";
                        $filehandle = opendir($ruta);
                          while ($file = readdir($filehandle)) {
                                if ($file != "." && $file != "..") {
                                    echo '<a href="'."/".$ruta."/".$file.'" class="col-md-4 col-xs-6" style="overflow: hidden; height: 300px; padding: 2px 4px;"> <img src="'."/".$ruta."/".$file.'" style="height: 100%;"/> </a>';
                                } 
                          } 
                        closedir($filehandle);
                    ?>
                </div>

               
            </div>
        </div>
    </section>

@endsection