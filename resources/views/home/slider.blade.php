<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="block wow fadeInUp" data-wow-delay=".3s">
                
                <!-- Slider -->
                <section class="cd-intro">
                    <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s" >
                    <span>Somos Editorial Flores Bustillo</span><br>
                    <span class="cd-words-wrapper">
                        <b class="is-visible">Promoviendo</b>
                        <b>Cultura</b>
                        <b>Costumbres</b>
                        <b>Tradiciones</b>
                    </span>
                    </h1>
                    </section> <!-- cd-intro -->
                    <!-- /.slider -->
                    <h2 class="wow fadeInUp animated" data-wow-delay=".6s" >
                        Más de 10 años de experiencia en investigación y promoción de la cultura salvadoreñas.
                    </h2>
                    <a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".9s" href="#feature" data-section="#feature" >Ver libros</a>
                    
                </div>
            </div>
        </div>
    </div>
</section><!--/#main-slider-->