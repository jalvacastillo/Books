<section id="feature">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">Departamentos</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                Conoce la cultura de El Salvador<br> Te compartimos toda la información de cada Departamento.
            </p>
        </div>
        <div class="row">
        @foreach($dep as $dep)
            <div class="col-md-3 col-xs-6">
                <div class="media wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="media-left">
                        <img src="images/departamentos/{{$dep['img']}}.jpg" alt="" width="100">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">{{$dep['nombre']}}</h4>
                        <p>
                            {{str_limit($dep['detalle'], $limit = 60, $end = '...')}}
                            <a class="btn" style="border: 1px solid; padding: 0 5px;" 
                            href="{{ route('culturadep', $dep['id'] ) }}">Ver</a>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
        </div>

        <div class="row">
            <div class="col-xs-4 col-xs-offset-4">
            <a class="wow fadeInUp animated btn btn-default btn-block animated" data-wow-delay=".9s">Ver más</a>
            </div>
        </div>
    </div>
</section>