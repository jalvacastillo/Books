<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
                    <h2>
                    Nosotros
                    </h2>
                    <p>
                       Nuestra empresa es de carácter socio cultural, único por su naturaleza en el quehacer del rescate y promoción de la cultura, costumbres y tradiciones, contribuyendo así a cultivar valores que fortalecen la identidad cuscatleca, que da frutos de unidad a nuestro El Salvador entre sus hijos que hacen país dentro y fuera de nuestras fronteras
                    </p>
                </div>
                
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">
                    <img src="images/about.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section> <!-- /#about -->