<section id="works" class="works">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">Libros</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                Descubre lo bonito de El Salvador <br> Conoce su historia, cultura y tradiciones.
            </p>
        </div>
        <div class="row">
        @foreach($libros as $libros)
            <div class="col-md-3  col-sm-6 col-xs-12">
                <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                    <div class="img-wrapper">
                        <img ng-src="images/libros/{{$libros['img']}}.jpg" class="img-responsive" alt="this is a title" >
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/libros/{{$libros['img']}}.jpg">Ver</a>
                                <a target="_blank" href="single-portfolio.html">Detalles</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                    <h4>
                    <a href="#">
                        {{$libros['nombre']}}
                    </a>
                    </h4>
                    <p>
                        {{$libros['titulo']}}
                    </p>
                    </figcaption>
                </figure>
            </div>
        @endforeach
        </div>
        <div class="row">
            <div class="col-xs-4 col-xs-offset-4">
            <a href="{{ route('tienda') }}" class="wow fadeInUp animated btn btn-default btn-block animated" data-wow-delay=".9s">Ver más</a>
            </div>
        </div>
    </div>
</section>