<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Contacto</title>
</head>
<style>
    body{
        font-family: verdana;
        font-size: 20px;
    }
    p{
        text-align: justify;
    }
</style>
<body>

    <h1>Saludos!</h1>
    <br><hr><br>
    <strong>Te ha escrito:</strong> {{ $cliente['nombre'] }} <br>
    <strong>Su correo es:</strong> {{ $cliente['email'] }} <br>
    <strong>Mensaje:</strong> <br>
    <p>
        {{ $cliente['mensaje'] }} <br>
    </p>

</body>
</html>