<div class="col-md-4">
    <div class="sidebar">

{{--         <div class="search widget">
            <form action="" method="get" class="searchform" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Buscar...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"> <i class="ion-search"></i> </button>
                    </span>
                </div>
            </form>
        </div> --}}

        <div class="author widget">
            <img class="img-responsive" src="images/author/author-bg.jpg">
            <div class="author-body text-center">
                <div class="author-img">
                    <img src="images/author/author.jpg">
                </div>
                <div class="author-bio">
                    <h3>Flores Bustillo</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt magnam asperiores consectetur, corporis ullam impedit.</p>
                </div>
            </div>
        </div>
        
        <div class="categories widget">
            <h3 class="widget-head">Departamentos</h3>
            <ul>
             @foreach($dep as $dep)
                <li>
                    <a href="{{ route('culturadep', $dep['id']) }}">{{$dep['nombre']}}</a> <span class="badge">></span>
                </li>
            @endforeach
            </ul>
        </div>
        
    </div>
</div>