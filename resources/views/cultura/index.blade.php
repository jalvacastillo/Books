@extends('base')

@section('content')



    @include('cultura.titulo')
    
    <section id="blog-full-width">
        <div class="container">
            <div class="row">

                    {{-- @include('cultura.sidebar') --}}
                    @include('cultura.departamentos')

            </div>
        </div>    
    </section>

@endsection