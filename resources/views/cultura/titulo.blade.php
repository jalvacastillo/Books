<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>Cultura</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ route('home') }}">
                                <i class="ion-ios-home"></i>
                                Inicio
                            </a>
                        </li>
                        <li class="active">Cultura</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>