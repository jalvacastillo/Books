@foreach($dep as $dep)
<div class="col-md-4">
    <article class="wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">
        <div class="blog-post-image">
            <a href="{{ route('culturadep', $dep['id']) }}">
            <div style="background-image: url(/images/departamentos/{{$dep['img']}}.jpg); width:100%; height:300px; background-position: center center;">
                
            </div>
            </a>
        </div>
        <div class="blog-content">
            <h2 class="blogpost-title">
            <a href="{{ route('culturadep', $dep['id']) }}">{{$dep['nombre']}}</a>
            </h2>
            <div class="blog-meta">
                <span>2016</span>
                <span>by <a href="">Flores Bustillo</a></span>
            </div>
            <p>
                {{str_limit($dep['detalle'], $limit = 60, $end = '...')}}
            </p>
            <a href="{{ route('culturadep', $dep['id']) }}" class="btn btn-dafault btn-details">Continuar Leyendo</a>
        </div>
    </article>
</div>
@endforeach