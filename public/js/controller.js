"use strict";

angular.module('app', [])

.constant("config", {
    "url": "http://localhost:8000/"
})

.controller('EmailCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {
    $scope.email = function(correo){
        console.log(correo);
        if($scope.correo){
            $scope.loader = true;
            $http.post(config.url + 'correo', $scope.correo).
              success(function(data, status) {
                if (status == 200) {
                    $scope.correo = {};
                    $scope.loader = false;
                    $scope.emailErrores = "Mensaje Enviado: Gracias por escribirnos";
                }else{
                    $scope.loader = false;
                    $scope.emailErrores = data;
                }
              }).
              error(function(data, status) {
                $scope.loader = false;
                $scope.emailErrores = "El mensaje no pudo ser enviado, revice su conexión a internet.";
              });
        }
    }
}])

.controller('HomeCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.libros = [];
    $scope.info = {};

    $http.get('/data/libros.json').then(
        function(data){ 
            $scope.libros = data.data;
            console.log(data);
        }, function(data){
            console.log(data);
        }
    );

    $http.get('/data/info.json').then(
        function(data){ 
            $scope.info = data.data[0];
            console.log(data);
        }, function(data){
            console.log(data);
        }
    );

   
        
}])


.controller('VideosCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.videos = [];

    $http.get('http://localhost:8000/api/videos').then(
        function(data){ 
            $scope.videos = data.data;
            console.log(data.data);
        }, function(data){
            console.log(data);
        }
    );
        
}])

.directive('video', function () {
    return {
        restrict: 'A',
        link: function postLink(scope, element, attr) {
            element.attr('src', 'https://www.youtube.com/embed/' + attr.dynamicUrlSrc);
        }
    };
});