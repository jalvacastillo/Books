<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{   
    static $array = array
                (
                    [
                        'id' => '1',
                        'nombre' => 'Cabañas',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'cabanas'
                    ],
                    [
                        'id' => '2',
                        'nombre' => 'Santa Ana',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'santa-ana'
                    ],
                    [
                        'id' => '3',
                        'nombre' => 'Cuscatlan',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'cuscatlan'
                    ],
                    [
                        'id' => '4',
                        'nombre' => 'La Unión',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'la-union'
                    ],
                    [
                        'id' => '5',
                        'nombre' => 'La Paz',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'la-paz'
                    ],
                    [
                        'id' => '6',
                        'nombre' => 'San Miguel',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'san-miguel'
                    ],
                    [
                        'id' => '7',
                        'nombre' => 'San Salvador',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'san-salvador'
                    ],
                    [
                        'id' => '8',
                        'nombre' => 'Ahuachapán',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'ahuachapan'
                    ],
                    [
                        'id' => '9',
                        'nombre' => 'Chalatenango',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'chalatenango'
                    ],
                    [
                        'id' => '10',
                        'nombre' => 'La Libertad',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'la-libertad'
                    ],
                    [
                        'id' => '11',
                        'nombre' => 'Morazán',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'morazan'
                    ],
                    [
                        'id' => '12',
                        'nombre' => 'San Vicente',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'san-vicente'
                    ],
                    [
                        'id' => '13',
                        'nombre' => 'Sonsonate',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'sonsonate'
                    ],
                    [
                        'id' => '14',
                        'nombre' => 'Usulután',
                        'titulo' => 'Sus pueblos, historia, cultura, tradiciones y más.',
                        'img' => 'usulutan'
                    ]
                );

    public static function getAll(){

     return self::$array;
        
    }

    public static function getOne($id){

        return self::$array[$id-1];

    }

    public static function getOnly($num){

        return array_slice(self::$array,0, $num);

    }

}
