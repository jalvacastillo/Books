<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{   
    static $array = array
                (
                    [
                        'id' => '1',
                        'nombre'=> 'Cabañas',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga nulla est molestias incidunt repellendus praesentium accusantium quidem nisi, voluptates aliquid debitis ut dolores, veniam enim eaque animi, soluta ipsa laboriosam.',
                        'img'=> 'cabanas'

                    ],
                    [
                        'id' => '2',
                        'nombre'=> 'Santa Ana',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam aperiam, fugit iusto, vero minus voluptas, obcaecati earum nostrum cupiditate culpa at aut incidunt dolores. Id repellat iusto explicabo quas rerum.',
                        'img'=> 'santa-ana'

                    ],
                    [
                        'id' => '3',
                        'nombre'=> 'Cuscatlan',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores quasi, dicta aliquid odit quidem dignissimos, unde temporibus tempore sapiente beatae quibusdam quas molestias necessitatibus iste et assumenda, aut fugiat eum.',
                        'img'=> 'cuscatlan'

                    ],
                    [
                        'id' => '4',
                        'nombre'=> 'La Unión',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae earum recusandae cupiditate atque laborum, magni dolorem consequatur sunt quaerat voluptate iste est tempora deleniti eum quia doloremque ea impedit, odit?',
                        'img'=> 'la-union'

                    ],
                    [
                        'id' => '5',
                        'nombre'=> 'La Paz',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae adipisci ex error veritatis fuga cupiditate et consectetur neque eligendi, sed earum hic similique ad ipsam tempora dicta repellendus nemo enim.',
                        'img'=> 'la-paz'

                    ],
                    [
                        'id' => '6',
                        'nombre'=> 'San Miguel',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ratione tempore commodi. Consequatur eveniet alias repudiandae sint nemo, facere, eaque animi nisi accusantium, explicabo repellendus ratione molestiae similique vero amet.',
                        'img'=> 'san-miguel'

                    ],
                    [
                        'id' => '7',
                        'nombre'=> 'San Salvador',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis similique cum, suscipit odit illo temporibus? Veniam magnam, dicta quisquam tenetur laboriosam. Quisquam officiis consectetur reprehenderit placeat labore deserunt, libero dolore!',
                        'img'=> 'san-salvador'

                    ],
                    [
                        'id' => '8',
                        'nombre'=> 'Ahuachapán',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus numquam quos iusto sint voluptate minus, commodi alias facere, voluptatem, atque eum at! Neque consectetur, blanditiis sit aliquam eius quod laudantium.',
                        'img'=> 'ahuachapan'

                    ],
                    [
                        'id' => '9',
                        'nombre'=> 'Chalatenango',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sequi minus exercitationem labore incidunt nostrum, fuga nihil aliquid. Maxime quia laboriosam debitis accusantium ducimus temporibus sed, similique ab. Dicta, illo.',
                        'img'=> 'chalatenango'

                    ],
                    [
                        'id' => '10',
                        'nombre'=> 'La Libertad',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta voluptatum laborum, praesentium debitis. Enim minima quos possimus, est ea error illo, incidunt hic rem reiciendis provident nobis dicta quae optio.',
                        'img'=> 'la-libertad'

                    ],
                    [
                        'id' => '11',
                        'nombre'=> 'Morazán',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe fuga iste totam minus, reiciendis eius, error sed perspiciatis doloribus autem eaque dolorum perferendis ad id facilis delectus dignissimos quo dolores?',
                        'img'=> 'morazan'

                    ],
                    [
                        'id' => '12',
                        'nombre'=> 'San Vicente',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos explicabo recusandae ratione neque vitae consectetur. Minus voluptatem itaque quia nulla, enim a in molestiae veritatis quibusdam ullam repudiandae, unde consequuntur.',
                        'img'=> 'san-vicente'

                    ],
                    [
                        'id' => '13',
                        'nombre'=> 'Sonsonate',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa modi aspernatur sint minus, ratione et, assumenda tempore inventore, suscipit praesentium cupiditate rem laudantium a. Fugit maxime dolore laudantium asperiores, quaerat!',
                        'img'=> 'sonsonate'

                    ],
                    [
                        'id' => '14',
                        'nombre'=> 'Usulután',
                        'detalle'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, voluptas ducimus dolorum non, praesentium eos aliquid a aliquam libero, repellat tenetur officia deserunt ex error veniam quidem, distinctio nulla totam.',
                        'img'=> 'usulutan'

                    ]

                );

    public static function getAll(){

     return self::$array;
        
    }

    public static function getOne($id){

        return self::$array[$id-1];

    }

    public static function getOnly($num){

        return array_slice(self::$array,0, $num);

    }

}
