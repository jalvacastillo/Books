<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Mail;
use Illuminate\Support\Collection;

use App\Departamento;
use App\Libro;

use Alaouy\Youtube\Youtube;

class HomeController extends Controller
{
    
    public function index(){

        $dep = Departamento::getOnly(4);
        $libros = Libro::getOnly(4);

        return view('index', compact('dep', 'libros'));
    }

    public function nosotros(){
        return view('nosotros.index');
    }

    public function cultura(){
         $dep = Departamento::getAll();

        return view('cultura.index', compact('dep'));
    }

    public function culturadep($id){ 

        $dep = Departamento::getOne($id);

        return view('cultura.departamento', compact('dep'));
    }

    public function tienda(){
        $libros = Libro::getAll();

        return view('tienda.index', compact('libros'));
    }

    public function fotos(){
        return view('galeria.fotos');
    }

    public function videosjson(){


        $youtube    = new Youtube('AIzaSyBVksDGrlASghDx_WRO0WLvu9cPZjFHLso');
        $videos     = $youtube->searchChannelVideos('','UCQdUPZIbHUgqEC3G4TVyztg',10);

        return Response::json($videos, 200, array('content-type' => 'application/json', 'Access-Control-Allow-Origin' => '*'));
    }

    public function videos(){

        return view('galeria.videos');
    }

    public function contactos(){
        return view('contactos.index');
    }

    public function correo(Request $Request)
    {   

        try {
            Mail::send('emails.contacto', ['cliente' => $Request], function ($m) use ($Request) {
                $m->from('alvarado.websis@gmail.com', 'Jesús Alvarado')
                  ->to('alvarado.websis@gmail.com', 'Jesús Alvarado')
                  ->subject('Página Web');
            });
            
            $msj = 'Mensaje enviado';
            return view('contactos.index' , compact('msj'));

        } catch (Exception $e) {
            $msj = 'No se pudo enviar el Mensaje';
            return view('contactos.index' , compact('msj'));
        }

    }
}
