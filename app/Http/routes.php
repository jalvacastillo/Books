<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {return view('index'); });


Route::get('/', 'HomeController@index')->name('home');
Route::get('/nosotros', 'HomeController@nosotros')->name('nosotros');
Route::get('/cultura', 'HomeController@cultura')->name('cultura');
Route::get('/culturas/{id}', 'HomeController@culturadep')->name('culturadep');
Route::get('/tienda', 'HomeController@tienda')->name('tienda');
Route::get('/galeria/fotos', 'HomeController@fotos')->name('fotos');
Route::get('/galeria/videos', 'HomeController@videos')->name('videos');
Route::get('api/videos', 'HomeController@videosjson');
Route::get('/contactos', 'HomeController@contactos')->name('contactos');

Route::post('/correo', 'HomeController@correo');